swagger: "2.0"
info:
  description: "This is a quick example of what the Scrum.org PST API documentation could look like"
  version: "-1.0.0"
  title: "Scrum.org PST API"
  termsOfService: "https://scrum.org/trainers"
  contact:
    email: "eric@scrum.org"
host: "www.scrum.org"
basePath: "/"
tags:
- name: "courses"
  description: "Everything about managing your courses"
schemes:
- "https"

paths:
  /admin/courses/manage:
    post:
      tags:
      - "login"
      summary: "Authentication solution"
      description: "Use normal credentials and re-use cookie for later requests"
      operationId: "login"
      produces:
      - "text/html"
      consumes:
      - "application/x-www-form-urlencoded"
      parameters:
      - name: "name"
        in: "formData"
        description: "username"
        required: true
        type: "string"
      - name: "pass"
        in: "formData"
        description: "password"
        required: true
        type: "string"
      - name: "op"
        in: "formData"
        description: "must be there, with value `Go`"
        required: true
        type: "string"
      - name: "form_id"
        in: "formData"
        description: "must be there, with value `user_login_form`"
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"
    get:
      tags:
      - "courses"
      summary: "Returns an HTML page with the 30 latest courses, taught by the logged in user"
      operationId: "listCoursesHtml"
      produces:
      - "text/html"
      parameters: []
      responses:
        200:
          description: "successful operation"

  /node/{courseId}/students:
    get:
      tags:
      - "students"
      summary: "Returns a (HTML wrapped) list of all students in a course"
      description: "Multiple status values can be provided with comma separated strings"
      operationId: "listStudentsHtml"
      produces:
      - "text/html"
      parameters:
      - name: "courseId"
        in: "path"
        description: "Id of the course where the student is attending"
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"

  /node/{courseId}/student:
    post:
      tags:
      - "students"
      summary: "Adds a student to a course"
      description: "Very nice feature, but NOT idempotent - calling twice = two entries"
      operationId: "addStudentToCourse"
      produces:
      - "application/json"
      parameters:
      - name: "courseId"
        in: "path"
        description: "Id of the course where the student is attending"
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"

  /node/{courseId}/student/{studentId}:
    delete:
      tags:
      - "students"
      summary: "Deletes a student from the list of students for a class"
      description: "-"
      operationId: "deleteStudentFromCourse"
      produces:
      - "application/json"
      parameters:
      - name: "courseId"
        in: "path"
        description: "Id of the course where the student is attending"
        required: true
        type: "string"
      - name: "studentId"
        in: "path"
        description: "Id of the student to remove"
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"

  "/trainers/score-report":
    get:
      tags:
      - "score report"
      summary: "returns an excel class score report"
      description: "returns data on how students in a class scored on associated assessment"
      operationId: "getScoreReport"
      produces:
      - "application/x-octet"
      parameters:
      - name: "courseId"
        in: "query"
        description: "this ID is found in the course metadata BUT IS NOT the same as node ID :-/"
        required: true
        type: "integer"
      responses:
        200:
          description: "successful operation"

definitions:
  Course:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      courseType:
        type: "integer"
        format: "int64"
      quantity:
        type: "integer"
        format: "int32"
      shipDate:
        type: "string"
        format: "date-time"
      status:
        type: "string"
        description: "Order Status"
        enum:
        - "placed"
        - "approved"
        - "delivered"
      complete:
        type: "boolean"
        default: false
    xml:
      name: "Order"
